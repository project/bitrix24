<?php

/**
 * Ubercart.
 * @param stdClass $order
 */
function bitrix24_forms__uc_processing($order) {
	$order->billing_country_code = '';
	$order->billing_zone_code = '';
	//get country and state
	if ($order->billing_country) {
		$result = db_query("SELECT * FROM {uc_countries} WHERE country_id = :id", array(':id' => $order->billing_country));
		if ($country = $result->fetchObject()) {
			$order->billing_country = $country->country_name;
			$order->billing_country_code = $country->country_iso_code_2;
		}
		if ($order->billing_zone) {
			$result = db_query("SELECT * FROM {uc_zones} WHERE zone_id = :id", array(':id' => $order->billing_zone));
			if ($zone = $result->fetchObject()) {
				$order->billing_zone = $zone->zone_name;
				$order->billing_zone_code = $zone->zone_code;
			}
		}
	}
	//send data
	B24Connector::getCurrent()->sendActivity(array(
		'AGENT' => array(
			'ORIGIN_ID' => $order->uid ? $order->uid : md5($order->primary_email),
			'NAME' => $order->billing_first_name ? $order->billing_first_name : 'Guest',
			'LAST_NAME' => $order->billing_last_name ? $order->billing_last_name : 'Guest',
			'PHONE' => $order->billing_phone,
			'EMAIL' => $order->primary_email,

			'ADDRESS_COUNTRY_CODE' => $order->billing_country_code,
			'ADDRESS_COUNTRY' => $order->billing_country,
			'ADDRESS_CITY' => $order->billing_city,
			'ADDRESS_POSTAL_CODE' => $order->billing_postal_code,
			'ADDRESS_PROVINCE' => $order->billing_zone,
			'ADDRESS' => $order->billing_street1 . "\n" . $order->billing_street2
		),
		'ACTIVITY' => array(
			'ORIGIN_ID' => $order->order_id,
			'NUMBER' => $order->order_id,
			'SUBJECT' => 'New order',
			'DESCRIPTION' => 'New order',
			'RESULT_SUM' => $order->order_total,
			'RESULT_CURRENCY_ID' => $order->currency,
			'EXTERNAL_URL' => '/admin/store/orders/' . $order->order_id,
		)
	));
}

/**
 * Drupal Commerce.
 * @param stdClass $order
 */
function bitrix24_forms__commerce_processing($order) {
	//base info
	$order_wrapper = entity_metadata_wrapper('commerce_order', $order->order_id);
	$billing_address = $order_wrapper->commerce_customer_billing->commerce_customer_address->value();
	$line_items = $order_wrapper->commerce_line_items;
	$total = commerce_line_items_total($line_items);
	if (empty($billing_address)) {
		return;
	}
	//country and state
	if (!isset($billing_address['country'])) {
		$billing_address['country'] = '';
	}
	if (!isset($billing_address['administrative_area'])) {
		$billing_address['administrative_area'] = '';
	}
	if (strlen($billing_address['country']) == 2) {
		if (strlen($billing_address['administrative_area']) == 2) {
			module_load_include('inc', 'addressfield', 'addressfield.administrative_areas');
			$administrative_areas = addressfield_get_administrative_areas($billing_address['country']);
			$billing_address['administrative_area'] = $administrative_areas[$billing_address['administrative_area']];
		}
		$country = _addressfield_country_options_list();
		$billing_address['country_name'] = $country[$billing_address['country']];
	}

	//send data
	B24Connector::getCurrent()->sendActivity($r=array(
		'AGENT' => array(
			'ORIGIN_ID' => $order->uid ? $order->uid : md5($order->mail),
			'NAME' => $billing_address['first_name'] ? $billing_address['first_name'] : 'Guest',
			'LAST_NAME' => $billing_address['last_name'] ? $billing_address['last_name'] : 'Guest',
			'EMAIL' => $order->mail,

			'ADDRESS_COUNTRY_CODE' => $billing_address['country'],
			'ADDRESS_COUNTRY' => $billing_address['country_name'],
			'ADDRESS_CITY' => $billing_address['locality'],
			'ADDRESS_POSTAL_CODE' => $billing_address['postal_code'],
			'ADDRESS_PROVINCE' => $billing_address['administrative_area'],
			'ADDRESS' => $billing_address['thoroughfare'] . "\n" . $billing_address['premise']
		),
		'ACTIVITY' => array(
			'ORIGIN_ID' => $order->order_id,
			'NUMBER' => $order->order_id,
			'SUBJECT' => 'New order',
			'DESCRIPTION' => 'New order',
			'RESULT_SUM' => $total['amount'] / 100,
			'RESULT_CURRENCY_ID' => $total['currency_code'],
			'EXTERNAL_URL' => '/products#overlay=admin/commerce/orders/' . $order->order_id . '/edit',
		)
	));
}


/**
 * Bitrix24 connector's gate.
 *
 * @version     1.0.0
 * @author      Bitrix24
 * @copyright   2016 Bitrix24
 * @link        https://bitrix24.com
 */
class B24Gate
{

	/**
	 * Save config.
	 * @param string $name
	 * @param string $value
	 */
	public static function saveConfig($name, $value)
	{
		variable_set($name, $value);
	}

	/**
	 * Get config.
	 * @param string $name
	 * @return mixed
	 */
	public static function getConfig($name)
	{
		return variable_get($name);
	}

	/**
	 * Get title of connector.
	 * @return string
	 */
	public static function getConnectorName()
	{
		return variable_get('site_name', 'Drupal');
	}

	/**
	 * Get id of connector.
	 * @return string
	 */
	public static function getConnectorId()
	{
		return 'DRUPAL';
	}

	/**
	 * Get host of connector.
	 * @return string
	 */
	public static function getConnectorHost()
	{
		global $base_url;
		return $base_url ? $base_url : 'http://localhost';
	}
}
