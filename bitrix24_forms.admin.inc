<?php

/*
 * Config form.
 */
function bitrix24_forms_settings($form) {
	$form = array();
	$form['bitrix24_forms_show_chat'] = array(
		'#type' => 'select',
		'#title' => t('Enable chat widget'),
		'#options' => array(
			'0' => t('No'),
			'1' => t('Yes'),
		),
		'#default_value' => variable_get('bitrix24_forms_show_chat', '0'),
		'#required' => false,
	);
	$form['bitrix24_forms_chat_code'] = array(
		'#type' => 'textarea',
		'#title' => t('Widget code'),
		'#default_value' => variable_get('bitrix24_forms_chat_code', ''),
		'#required' => false,
	);
	$form['b24c_enable'] = array(
		'#type' => 'select',
		'#title' => t('Enable CRM connector'),
		'#options' => array(
			'0' => t('No'),
			'1' => t('Yes'),
		),
		'#default_value' => variable_get('b24c_enable', '0'),
		'#required' => false,
	);
	$form['b24c_portal'] = array(
		'#type' => 'textfield',
		'#title' => t('Connector URL'),
		'#default_value' => variable_get('b24c_portal', ''),
		'#required' => false,
	);

	return system_settings_form($form);
}